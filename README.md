
Books To Scrape
===============

This project has the ultimate goal of extracting specific data using the ETL process from all product pages on the website [books.toscrape.com](http://books.toscrape.com/).

Current Progress
----------------

At present, the script generates 2 folders named "data" and "image" and an log file "scrapping.log" from the current folder from which it is executed, then extracts the data from each product page to load it into a csv file named after the category browsed, and downloads the images from each book to store them in the image folder, each image itself being saved in a folder named after the category to which it belongs.


Prerequisites
-------------

- Python3 installed
- Create virtual environment 
- Clone the project and install the packages specified in the requirements.txt file


Creating virtual environment, import of repo and script execution
-----------------------------------------------------------------

_Windows_ :

```
# Start here if you are not already in the folder where you will import the repository
cd \my\directory\path

# Start from here if you are already inside the folder that will contain the repository
git clone https://gitlab.com/Hekynox/books_toscrape.git
python -m venv env
.\env\Scripts\activate
pip install -r requirements.txt
python.exe .\scrapping.py
```

_Linux_ :

```
# Start here if you are not already in the folder where you will import the repository
cd /my/directory/path

# Start from here if you are already inside the folder that will contain the repository
git clone https://gitlab.com/Hekynox/books_toscrape.git
python -m venv env
source env/bin/activate
pip install -r requirements.txt
chmod +x ./scrapping.py
python3.10 ./scrapping.py
```

Information
-----------

The README file will be updated throughout the project's progress to remain consistent with its evolution.

\
*__French Version Below__*
--------------------------
\
Books To Scrape
===============

Ce projet a pour objectif final d'extraire des données spécifiques à l'aide du processus ETL à partir de toutes les pages de produits du site web [books.toscrape.com](http://books.toscrape.com/).

Progression actuelle
--------------------

Actuellement, le script genère 2 dossiers nommé "data" et "image" et un fichier de log "scrapping.log" à partir du dossier courant depuis lequel il est executé, puis extrait les données de chaque page produit pour les charger dans un fichier csv portant le nom de la catégorie parcourue et telecharge les images de chaque livre pour les stocker dans le dossier image, chaque image étant enregistrée elle même dans un dossier portant le nom la catégorie à laquelle elle appartient. 

Prérequis
---------

- Python3 ou version ultérieur installée
- Environnement virtuel créé
- Cloner le projet et installer les paquets spécifiés dans le fichier requirements.txt

Création de l'environnement virtuel, import du repo et exécution du script
--------------------------------------------------------------------------

_Windows_ :

```
# Commencer ici si vous n'êtes pas déjà dans le dossier dans lequel vous allez importer le repository
cd \mon\chemin\dossier

# commencer à partir d'ici si vous vous trouvez déjà dans le dossier qui va contenir le repository
git clone https://gitlab.com/Hekynox/books_toscrape.git
python -m venv env
.\env\Scripts\activate
pip install -r requirements.txt
python.exe .\scrapping.py
```

_Linux_ :

```
# Commencer ici si vous n'êtes pas déjà dans le dossier dans lequel vous allez importer le repository
cd /mon/chemin/dossier

# commencer à partir d'ici si vous vous trouvez déjà dans le dossier qui va contenir le repository
git clone https://gitlab.com/Hekynox/books_toscrape.git
python -m venv env
source env/bin/activate
pip install -r requirements.txt
chmod +x ./scrapping.py
python3.10 ./scrapping.py
```

Information
-----------

Le fichier README sera mis à jour tout au long de l'avancement du projet afin de rester cohérent avec son évolution.