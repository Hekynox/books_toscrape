#!/usr/bin/env python3
# coding: utf-8
"""
This script Extract, transform, and load (ETL) data from a specified category on : 
'http://books.toscrape.com' and create folder for data extracted named "data" and
another one for image downloaded named "image"

It then executes the ETL process, extracting the data from each product page
in the specified category, transforms it and loads it into a CSV file and
download image appropriate for each product page.

One CSV will be generated for appropriate category.
"""

from bs4 import BeautifulSoup
from urllib.parse import urljoin
from datetime import datetime
import requests
import csv
import logging
import argparse
import os


def extract_data(soup):
    """
    Extract data from a product page.

    Parameters:
    - soup (Beautifulsoup): The Beautifulsoup object representing the parsed
    HTML of the product page.

    Returns:
    Tuple of lists containing extracted data:
    - product_page_url (list): empty list for using later.
    - upc_list (list): Universal Product Codes.
    - titles (list): Product titles.
    - priceTax (list): Prices including tax.
    - priceNoTax (list): Prices excluding tax.
    - nbrAvailable (list): Product availables
    - product_description (list): Product descriptions.
    - category (list): Product categories.
    - rating_list (list): Product ratings.
    - img_list (list): Image URLs.
    """
    # Variables for register extract data
    productPageUrl = []
    upcList = []
    priceTax = []
    priceNoTax = []
    nbrAvailable = []
    titles = []
    ratingList = []
    productDescription = []
    imgList = []
    category = []
    baseUrl = "https://books.toscrape.com"

    # Extract UPC, price, avaibility
    for i in soup.find_all("table", class_="table table-striped"):
        # Extract UPC
        for upc in i.find("td"):
            upcList.append(upc.get_text())

        # Extract price
        for target in i.find_all("th"):
            if target.get_text() == "Price (excl. tax)":
                priceNotTax = target.find_next("td")
                priceNoTax.append(priceNotTax.get_text())
            if target.get_text() == "Price (incl. tax)":
                priceWithTax = target.find_next("td")
                priceTax.append(priceWithTax.get_text())

            # Extract availability
            if target.get_text() == "Availability":
                available = target.find_next("td")
                nbrAvailable.append(available.get_text())

    for i in soup.find_all("div", class_="col-sm-6 product_main"):
        # Extract title
        for titleBook in i.find_all("h1"):
            titles.append(titleBook.get_text())

        # Extract rating
        starRating = {"One": "1", "Two": "2", "Three": "3", "Four": "4", "Five": "5"}
        for starRate in starRating.keys():
            star = i.find("p", class_=f"star-rating {starRate}")
            if star:
                finalStar = starRating.get(starRate)
                ratingList.append(f"Rating : {finalStar}/5")

    # Extract description
    if soup.find_all("div", id="product_description"):
        for i in soup.find_all("div", id="product_description"):
            desc = i.find_next("p")
            productDescription.append(desc.get_text())
    else:
        productDescription.append("No description available")

    # Extract image URL
    for i in soup.find_all("div", class_="item active"):
        for img in i.find_all("img"):
            imgUrl = img["src"]
            finalUrl = urljoin(baseUrl, imgUrl)
            imgList.append(finalUrl)

    # Extract Category
    for i in soup.find_all("li", class_="active"):
        for prev in i.find_previous("a"):
            theCategory = prev.get_text()
            category.append(theCategory)
    return (
        productPageUrl,
        upcList,
        titles,
        priceTax,
        priceNoTax,
        nbrAvailable,
        productDescription,
        category,
        ratingList,
        imgList,
    )


def data_load(dataList, catFile, nameImgList, urlImageList, session):
    """
    Load data extracted from a list into a CSV file and download a book image and add
    it to an "image" folder.

    If the image category folder already exists, add the image to this folder,
    otherwise create it.

    The CSV files are generated in the 'data' folder and are named according to 
    the corresponding category with the date and time of file generation

    Parameters:

    - data_list(list): Data to be loaded into the CSV file.
    Each element in the list represents a row in the CSV file.
    - cat_file(str): Using for etablished a name of CSV file and the image folder
    - nameImgList(list): Name of image
    - urlImageList(list): URLs of image
    - session(Session): Session etablished with the web site
    """
    en_tete = [
        "product_page_url",
        "universal_product_code (upc)",
        "title",
        "price_including_tax",
        "price_excluding_tax",
        "number_available",
        "product_description",
        "category",
        "review_rating",
        "image_url",
    ]
    indiceNameimg = 0

    dateTime = datetime.now().strftime("%Y-%m-%d_%H-%M")
    categoryFolder = os.path.join("data")
    os.makedirs(categoryFolder, exist_ok=True)
    dataPath = os.path.join(categoryFolder, f"{catFile}_{dateTime}.csv")

    with open(dataPath, "w", encoding="utf-8", newline="") as csv_file:
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(en_tete)
        for elem in dataList:
            writer.writerow(elem)
    
    for i in urlImageList:
        response = session.get(i)
        categoryFolderImg = os.path.join("image", catFile)
        os.makedirs(categoryFolderImg, exist_ok=True)
        image_path = os.path.join(categoryFolderImg, nameImgList[indiceNameimg])
        indiceNameimg += 1
        with open(image_path, "wb") as file:
            file.write(response.content)


def category_finder(soup):
    """
    Extracts and return a list of URLs for category page from a website.

    Parameters:
    - soup (Beautifulsoup): The soup object representig the parsed HTML content of
    the main page from this website : http://books.toscrape.com/

    Returns:
    - list: Category URLs
    """
    categoryList = []
    url = "http://books.toscrape.com"
    for i in soup.find_all("ul", class_="nav nav-list"):
        for x in i.find_all("ul"):
            for a in x.find_all("a"):
                tagHref = a.get("href")
                urlCompl = urljoin(url, tagHref)
                categoryList.append(urlCompl)
    return categoryList


def url_book_cat(soup):
    """
    Extracts and return a list of URLs for book pages from a category page and
    their image url.

    Parameters:
    - soup (Beautifulsoup): The soup object representing the parsed HTML
    content of the category page.

    Returns:
    - list: URLs for individual book pages in the specified category.
    - list: URLs image find on page
    """
    prodPageUrl = []
    imgUrl = []
    baseUrl = "http://books.toscrape.com/"
    for i in soup.find_all("ol", class_="row"):
        for h3 in i.find_all("div", class_="image_container"):
            for a in h3.find_all("a"):
                tagHref = a.get("href")
                tagHref = tagHref.replace("../../../", "catalogue/")
                finalUrl = urljoin(baseUrl, tagHref)
                prodPageUrl.append(finalUrl)

            for img in h3.find_all("img", class_="thumbnail"):
                tagImg = img.get("src")
                tagImgUrl = urljoin(baseUrl, tagImg)
                imgUrl.append(tagImgUrl)
    return prodPageUrl, imgUrl


def main_session():
    """
    Etablished session with web site for using at all interaction with it
    in the script.

    Returns:
    - session(Session): Session etablished with the web site
    """
    session = requests.session()
    return session


def sauce(url, session):
    """
    Extracts and returns Beautifulsoup soup object from the provided URL 
    and URL page.

    Parameters :
    - url(str): The URL of the page to be scraped
    - session(Session): Session etablished with the web site

    Returns :
    - Beautifulsoup: The soup object representing the parsed HTML content.
    - string: URL of page
    """
    response = session.get(url)
    urlPage = response.url
    page = response.content
    soup = BeautifulSoup(page, "html.parser")
    return soup, urlPage


def next_page(soup, urlPage):
    """
    Searches for a next link on a web page and returns the full URL of the next
    page if there one, otherwise returns 'NotPage'.

    Parameters:
    - soup (Beautifulsoup): The soup object representing the parsed HTML content
    of the category page
    - url_page: URL of page

    Returns:
    - string: If it finds a URL for next page on the page, it returns the complete
    URL, otherwise, it returns 'NotPage'.
    """
    nextUrl = "NotPage"
    for i in soup.find_all("li", class_="next"):
        for a in i.find_all("a"):
            tagHref = a.get("href")
            reponse = requests.get(urlPage)
            currentUrl = reponse.url
            nextUrl = urljoin(currentUrl, tagHref)
    return nextUrl


def filename_img(nameImg):
    """
    Modify string for exclude already special caracteres which comes into conflict
    when it use for name file once registred on operating system.

    Parameters:
    - nameImg (str): Name of image

    Returns: 
    - string: Name of image formatted without special caracters
    """
    caracList = ["/", "//", ":", '"',"'","*", ".", "?"]
    extensionImg = ".jpg"
    for i in caracList:
        if i in nameImg:
            nameImg = nameImg.replace("/", "_")
            nameImg = nameImg.replace(":", " -")
            nameImg = nameImg.replace('"', "")
            nameImg = nameImg.replace("'", "")
            nameImg = nameImg.replace("*", "°")
            nameImg = nameImg.replace(".", "")
            nameImg = nameImg.replace("?", "")
    nameImg = f"{nameImg}{extensionImg}"
    return nameImg


def transform(list):
    """
    Transforms a list to make it usable for extracting data from product page
    in to a list, and then transforms this list so that the data is organized
    into a single and usable for loading data into a CSV file.

    Also uses the list of image URLs to match each image to the correct book 
    and transform its name to correspond to it.

    Parameters:
    - liste (list): Initial list to be transformed

    Returns:
    - finalList(list): Sorted list of a data from product pages, each list in the list
    represents a product page
    - listName(list): Title of books for using later as name 
    of image downloaded
    - titlecsv(str): contains a name of category for using later during the load data
    """
    dataListSort = []
    finalList = []
    listName = []
    lenSubList = 10

    for incr in list:
        for app in incr:
            for last in app:
                dataListSort.append(last)
    # Traverse the list with a step of 10
    # At the first iteration, i = 0
    for i in range(0, len(dataListSort), lenSubList):
        # Record the 10 elements traversed in the subList
        subList = dataListSort[i : i + lenSubList]
        # Add subList to the finalList
        finalList.append(subList)

    for titles in finalList:
        nameImg = titles[2]
        nameImg = filename_img(nameImg)
        listName.append(nameImg)
    titlecsv = finalList[0][7]
    return finalList, listName, titlecsv


def extract(liste, session):
    """
    Extract data of all the books of list url.

    Parameters:

    - liste(list): Books urls
    - session(Session): Session etablished with the web site
    """
    finalBookUrl = []
    dataList = []
    indiceTuple = 0
    indiceListe = 0
    for ex in liste:
    # Add each URL to a list
        finalBookUrl.append(ex)

    # Iterate over the list containing the book URLs of the category
    for i in finalBookUrl:
        # Create a soup for the current iteration of the book URL
        soupBooksUrl = sauce(i, session)
        soupBooks = soupBooksUrl[0]
        # Add the data of the currently iterated book to a list
        dataList.append(extract_data(soupBooks))
        list(dataList[0][0])
        # Add a product page url in a list of data extracted
        dataList[indiceTuple][indiceListe].append(i)
        indiceTuple += 1
    return dataList


def etl():
    """
    Extract, transform, and load (ETL) data from a specified category on :
    'http://books.toscrape.com'.

    It then performs the ETL process, extracting data from each product page
    in the specified category, transforming it, and loading it into a CSV file
    and images are loading in another folder.
    """

    parser = argparse.ArgumentParser(
        prog="scrapping.py", usage="%(prog)s [-option]", description=__doc__
    )

    args = parser.parse_args()

    logging.basicConfig(
        encoding="utf-8", format="%(asctime)s %(levelname)s %(message)s", 
        level=logging.DEBUG, filename="scrapping.log", datefmt="%Y-%m-%d %H:%M:%S"
    )

    logging.info("Scraping initiation")

    url = "http://books.toscrape.com"
    urlBookList = []
    urlImages = []
    urlBookListFinal = []
    titleCsv = ""
    mainSession = main_session()
    os.mkdir("image")
    os.mkdir("data")

    reponse = requests.get(url)
    if reponse.ok:
        logging.info("Scraping in progress, please wait.")
        soup = sauce(url, mainSession)
        soupE = soup[0]
        # Retrieve the list of category URLs
        listCategory = category_finder(soupE)

        # iIterate over the list of URLs from ListCategory
        for urlCat in listCategory:
            logging.debug("start to iterate on category list urls")
            # Establish the soup for the current iteration
            urlCatSoup = sauce(urlCat, mainSession)
            urlCatSoupE = urlCatSoup[0]
            
            # Create a list of each URL contained on the page
            urlBookList = url_book_cat(urlCatSoupE)
            for urls in urlBookList[0]:
                urlBookListFinal.append(urls)
            for imgs in urlBookList[1]:
                urlImages.append(imgs)

            # Add the list of URLs to another list, urlBookListFinal
            # Retrieve the URL of the next page from the list
            nextPage = next_page(urlCatSoupE, urlCat)
            # Check if a next page is present; if yes, navigate to it
            while nextPage != "NotPage":
                nextPageT = sauce(nextPage, mainSession)
                nextPageUrl = nextPageT[1]
                nextPageSoup = nextPageT[0]
                urlBookList = url_book_cat(nextPageSoup)
                for urlsX in urlBookList[0]:
                    urlBookListFinal.append(urlsX)
                for img in urlBookList[1]:
                    urlImages.append(img)
                nextPage = next_page(nextPageSoup, nextPageUrl)
                logging.debug("next page find or not in while etablished")

            # Extract the data from a category list of url
            dataList = extract(urlBookListFinal, mainSession)

            # Transform data for using at loading
            listTransformed = transform(dataList)
            finalList = listTransformed[0]
            nameImgList = listTransformed[1]
            titleCsv = listTransformed[2]

            # Load data
            data_load(finalList, titleCsv, nameImgList, urlImages, mainSession)
            logging.info(f"data and image loading for {titleCsv} category")

            finalList.clear()
            urlBookListFinal.clear()
            urlImages.clear()
        logging.info("Scraping finished")
    else:
        logging.critical("URL not found or not accessible")


if __name__ == '__main__':
    etl()